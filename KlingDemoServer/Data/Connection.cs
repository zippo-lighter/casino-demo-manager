﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace KlingDemoServer.Data
{
    public static class Connection
    {
        public static List<string> Ids = new List<string>();
    }
    public static class OnlineMachineList
    {
        public static List<ClientObject> Ids = new List<ClientObject>();

        public static void Remove(string contextID)
        {
            try { 
                var machineToErase = Ids.SingleOrDefault(m => m.ConnectionID == contextID);
                Ids.Remove(machineToErase);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static void ChangeMachineStatus(string contextID, bool block)
        {
            try
            {
                var machineToChange = Ids.SingleOrDefault(m => m.ConnectionID == contextID);
                if (block) {
                    machineToChange.IsBlocked = true;
                }
                else { machineToChange.IsBlocked = false; }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        
    }
    public static class MonitorClientList
    {
        public static List<ClientObject> Ids = new List<ClientObject>();

        public static void Remove(string contextID)
        {
            try
            {
                var machineToErase = Ids.SingleOrDefault(m => m.ConnectionID == contextID);
                Ids.Remove(machineToErase);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

    }
}
