﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection.PortableExecutable;

namespace KlingDemoServer.Data
{
    public class ClientObject
    {
        private static readonly string[] Names = new[]
        {
            "Linzgau", "Bregenz", "Ulm", "Baindt", "Ravensburg", "Lindau", "Meersburg", "Konstanz"
        };
        internal enum state { Disconnected, WaitForCode, InputCode, Play }

        private string _machineName;
        private HubConnection _hubConnection;
        public const string HUBURL = "/hub";
        private readonly NavigationManager _navigationManager;
        private string _macAddress;
        private string _connectionId;
        private bool _connected = false;
        private bool _isBlocked = false;
        private string machineState;

        public ClientObject(int namePosition)
        {
            _machineName = Names[namePosition];
        }
        public ClientObject(string connectionID)
        {
            _connectionId = connectionID;
        }
        public ClientObject(string connectionID, int namePosition)
        {
            _connectionId = connectionID;
            _machineName = Names[namePosition];
        }
        public string machineName
        {
            get { return _machineName; }
            set { _machineName = value; }
        }
        public string ConnectionID { get { return _connectionId; } set { _connectionId = value; } }
        public bool IsBlocked { get { return _isBlocked; } set { _isBlocked = value; } }
        public void allocate(string connectionId)
        {
            _connectionId = connectionId;
            _connected = true;
        }
        public void deallocate()
        {
            _connectionId = null;
            _connected = false;
        }

        private int getCharValue(string inputString)
        {
            int summatory = 0;
            for (int i = 0; i < inputString.Length; i++)
            {
                summatory += (int)inputString[i];
            }
            return summatory;
        }

        private string generateCode()
        {
            int output = getCharValue(_connectionId) + getCharValue(_machineName);
            while (output > 9999)
            {
                Console.WriteLine(output);
                output /= 10;
            }
            string toreturn = output.ToString();
            if (output < 1000)
            {
                do {
                    toreturn = '0' + toreturn;
                } while (toreturn.Length < 4);
            }
            return toreturn; ;
        }

        public string generateCode(string serverId)
        {

            int output = getCharValue(generateCode()) + getCharValue(serverId);
            while (output > 9999)
            {
                Console.WriteLine(output);
                output /= 10;
            }
            string toreturn = output.ToString();
            if (output < 1000)
            {
                do
                {
                    toreturn = '0' + toreturn;
                } while (toreturn.Length < 4);
            }
            return toreturn; ;
        }

        public bool Connected { get { return _connected; } set { _connected = value; } }
        public void connect()
        {
            if (!_connected) { 
                _connected = true;
            }
        }

       


    }
}
