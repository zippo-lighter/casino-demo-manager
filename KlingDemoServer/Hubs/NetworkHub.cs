﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using KlingDemoServer.Data;
using System.Collections.Generic;
using System.Linq;

namespace KlingDemoServer.Hubs
{
    public class NetworkHub : Hub
    { 
        
        public override async Task OnConnectedAsync()
        {
            Connection.Ids.Add(Context.ConnectionId);
            await base.OnConnectedAsync();
        }
        public override Task OnDisconnectedAsync(Exception exception)
        {
            try
            {
                var clientToRemove = MonitorClientList.Ids.SingleOrDefault(m => m.ConnectionID == Context.ConnectionId);
                var machineToRemove = OnlineMachineList.Ids.SingleOrDefault(m => m.ConnectionID == Context.ConnectionId);
                if (clientToRemove !=  null)
                {
                    MonitorClientList.Ids.Remove(clientToRemove);
                }
                else if (machineToRemove !=  null){
                    
                    OnlineMachineList.Ids.Remove(machineToRemove);
                }
                
            }
            catch (Exception e)
            {

                    Debug.Write(e.ToString()+"- Client not removed");

            }
            Connection.Ids.Remove(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }
        public async Task AddMachineToGroup(string groupName )
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            OnlineMachineList.Ids.Add(new ClientObject(Context.ConnectionId, OnlineMachineList.Ids.Count));
            await Clients.All.SendAsync("Send", "#n");
            await Clients.Group(groupName).SendAsync("Send", $"{Context.ConnectionId} has joined the group.");
        }
        public async Task AddMonitorClientToGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            MonitorClientList.Ids.Add(new ClientObject(Context.ConnectionId, MonitorClientList.Ids.Count));
            await Clients.Group(groupName).SendAsync("Send", $"{Context.ConnectionId} has joined the '"+groupName+"'group.");
        }
        public async Task RemoveMachineFromGroup(string groupName)
        {
            try
            {
                var machineToRemove = OnlineMachineList.Ids.SingleOrDefault(m => m.ConnectionID == Context.ConnectionId);
                OnlineMachineList.Ids.Remove(machineToRemove);
            }
            catch (Exception e)
            {
                Debug.Write("Machine not removed");
            }
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
            await Clients.Group(groupName).SendAsync("Send", $"{Context.ConnectionId} has left the group {groupName}.");
        }
        public async Task RemoveMonitorClientFromGroup(string groupName)
        {
            try
            {
                var machineToRemove = MonitorClientList.Ids.SingleOrDefault(m => m.ConnectionID == Context.ConnectionId);
                MonitorClientList.Ids.Remove(machineToRemove);
            }
            catch (Exception e)
            {
                Debug.Write("Machine not removed");
            }
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
            await Clients.Group(groupName).SendAsync("Send", $"{Context.ConnectionId} has left the group {groupName}.");
        }

        public Task SendMessage(string user, string message)
        {
            return Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public async Task SendPrivateMessage(string userId, string message)
        {
            //var message = $"Send message to you with user id {userId}";
            await Clients.Client(userId).SendAsync("Send", message);
        }

        public async Task SendMessageToGroup(string groupName, string message)
        {
            await Clients.Group(groupName).SendAsync("GroupMessage", message);
        }
        //public async void GeneratePassword(string user)
        //{
        //    //await Clients.Users<user>
        //}
    }
}
