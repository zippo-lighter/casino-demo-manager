﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using KlingDemoServer.Hubs;
using KlingDemoServer.Data;


namespace KlingDemoServer
{
    public class MachineManager : BackgroundService
    {
        private readonly IHubContext<NetworkHub> _hubContext;
        private readonly int maxNumberOfMachines = 8;
        public List<ClientObject> machines;
        

        public MachineManager(IHubContext<NetworkHub> hubContext)
        {
            _hubContext = hubContext;
            machines = new List<ClientObject>(maxNumberOfMachines);
            for (int i = 0; i<maxNumberOfMachines; i++)
            {
                machines.Add( new ClientObject(i));
            }
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            throw new NotImplementedException();
        }
    }
}
