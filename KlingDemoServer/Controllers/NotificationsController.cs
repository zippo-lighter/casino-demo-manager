﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KlingDemoServer.Hubs;
using KlingDemoServer.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace KlingDemoServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationsController : ControllerBase
    {
        private readonly IHubContext<NetworkHub> _hubContext;

        public NotificationsController(IHubContext<NetworkHub> hubContext)
        {
            _hubContext = hubContext;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]string machines)
        {
            await _hubContext.Clients.All.SendAsync("notification",  machines);

            return Ok("Machines List has been sent");
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromQuery]string title)
        {
            await _hubContext.Clients.All.SendAsync("notification", $"{DateTime.Now}: {title}");
            try { 
                await _hubContext.Clients.Group("Spielgeraete").SendAsync("SendMessageToGroup", "hey there!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return Ok("Notification has been sent successfully!");
        }

    }
}
